# Research_On_O2O_Takeaway_Delivery_Route_Optimization_Based_On_VRP_Model
基于VRP模型的O2O外卖配送路径优化研究

专业名词解释：  
1.VRP：车辆路径问题，即Vehicle Routing Problem。  
2.O2O:即Online To Offline，是指将线下的商务机会与互联网结合，让互联网成为线下交易的前台，这个概念最早来源于美国。O2O的概念非常广泛，只要产业链中既可涉及到线上，又可涉及到线下，就可通称为O2O。







