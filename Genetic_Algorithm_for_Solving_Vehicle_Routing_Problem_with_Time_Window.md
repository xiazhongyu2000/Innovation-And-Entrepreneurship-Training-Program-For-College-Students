# Genetic_Algorithm_for_Solving_Vehicle_Routing_Problem_with_Time_Window
遗传算法求解带时间窗的车辆路径问题  

带时间窗的车辆路径问题(Vehicle Routing Problem with Time Windows,VRPTW)是典型的组合优化问题，目前已经被广泛研究。VRPTW问题可以简单描述为：已知n个顾客和一个配送中心的x和y坐标、需求量和时间窗(如9:00~17:00)，由配送中心组织车队向顾客提供服务，在满足约束和所有顾客需求的前提下，尽可能使车队总成本最低(如最小化车辆行驶总距离、最小化车辆使用数目等)。  
VRPTW可定义在有向图 <img src="images/gva.svg" height="15"> ,其中 <img src="images/vnn.svg" height="15"> 表示所有节点的集合，0和 n+1 表示配送中心，<img src="images/12n.svg" height="15"> 表示顾客， *A* 表示弧的集合。规定在有向图 *G* 上，一条合理的配送路线必须始于节点0，终于节点 n+1 。VRPTW模型中涉及的参数如1表所示，决策变量如表2所示。此外， <img src="images/deltai.svg" height="15"> 表示从节点 <img src="images/l.svg" height="15"> 出发的弧的集合， <img src="images/deltaj.svg" height="15"> 表示回到节点 <img src="images/j.svg" height="15"> 的弧的集合， <img src="images/nvn.svg" height="15"> 表示顾客集合， *K* 表示配送车辆集合。  

表1 参数

| 变量符号 | 参数含义 |
| :------: | :------: |
| <img src="images/cij.svg" height="15"> | 表示节点 <img src="images/i.svg" height="15"> 和节点 <img src="images/j.svg" height="15"> 之间的距离 |
| <img src="images/v.svg" height="10"> | 配送车辆的行驶速度 |
| <img src="images/si.svg" height="15"> | 顾客 <img src="images/i.svg" height="15"> 的服务时间 |
| <img src="images/tij.svg" height="15"> | 从节点 <img src="images/i.svg" height="15"> 到节点<img src="images/j.svg" height="15"> 的行驶时间 |
| <img src="images/ai.svg" height="15"> | 顾客 <img src="images/i.svg" height="15"> 的左时间窗 |
| <img src="images/bi.svg" height="15"> | 顾客 <img src="images/i.svg" height="15"> 的右时间窗 |
| <img src="images/e.svg" height="15"> | 配送中心的左时间窗 |
| <img src="images/l.svg" height="15"> | 配送中心的右时间窗 |
| <img src="images/di.svg" height="15"> | 顾客 <img src="images/i.svg" height="15"> 的需求量 |
| <img src="images/c.svg" height="15"> | 货车最大装载量 |
| <img src="images/m.svg" height="15"> | 足够大的正数 |

表2 决策变量

| 变量符号 | 变量含义 |
| :------: | :------: |
| <img src="images/wik.svg" height="15"> | 车辆 <img src="images/k.svg" height="15"> 对节点<img src="images/i.svg" height="15"> 的开始服务时间 |
| <img src="images/xijk.svg" height="15"> | 货车 <img src="images/k.svg" height="15"> 是否从节点 <img src="images/i.svg" height="15"> 出发前往节点 <img src="images/j.svg" height="15"> ，如果是，则 <img src="images/xijk1.svg" height="15"> ,否则 <img src="images/xijk0.svg" height="15"> |

在构建VRPTW模型时，允许配送货车在顾客的左时间窗之前到达顾客，但需要等待至左时间窗才可以为顾客服务，不允许配送货车在顾客的右时间窗之后到达顾客。

综上所述，则VRPTW模型如下：  

<img src="images/minkkijacijxijk.svg" height="50">  (2.1)   

<img src="images/kkjixijk1in.svg" height="50">  (2.2)

<img src="images/j0xojk1kk.svg" height="50">  (2.3)

<img src="images/ijxijkijxjik0kkjn.svg" height="50">  (2.4)

<img src="images/in1xin1k1kk.svg" height="50"> (2.5)

<img src="images/tijcijv.svg" height="50"> (2.6)

<img src="images/wiksitijwjk1xijkmkkija.svg" height="25"> (2.7)

<img src="images/aijixijkwikbijixijkkkin.svg" height="50"> (2.8)

<img src="images/ewiklkki0n1.svg" height="25"> (2.9)

<img src="images/indijixijkckk.svg" height="50"> (2.10)

<img src="images/" height="50"> (2.11)


