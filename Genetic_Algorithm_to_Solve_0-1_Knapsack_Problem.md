# Genetic_Algorithm_to_Solve_0-1_Knapsack_Problem
遗传算法求解0-1背包问题  
0-1背包问题是易于理解且较为容易的组合优化问题。假设现有若干个物品，它们的质量和价值已知。此外，还有一个有承重质量限制的背包，则0-1背包问题可以简单地描述为：如何把这些物品放入这个有承重质量限制的背包中，在不超出背包最大承重限制的前提下，使得放入背包中的物品总价值最大。

遗传算法(Genetic Algorithm,GA)作为一种经典的智能优化算法，已被广泛应用于组合优化问题中。

问题描述：已知 <img src="images/l.svg" height="15"> 个物品的质量及其价值分别为 <img src="images/wiil.svg" height="20"> 和 <img src="images/viil.svg" height="20"> ,背包的最大载重量为 <img src="images/c.svg" height="15"> ,则0-1背包问题可被描述为：选择哪些物品装入背包，使在背包在最大载重量限制之内所装物品的总价值最大？  
因此，0-1背包问题的数学模型如下：  
<img src="images/maxvixi.svg" height="60"> (1.1)  
<img src="images/ilwxc.svg" height="60"> (1.2)  
<img src="images/xiil.svg" height="20"> (1.3)  
式中， <img src="images/xi.svg" height="15"> 为0-1决策变量，表示物品 <img src="images/i.svg" height="15"> 是否被装包，如果是，则 <img src="images/xi1.svg" height="15"> ,否则 <img src="images/xi0.svg" height="15"> 。  
目标函数(1.1)表示最大化背包中物品的总价值；约束(1.2)限制装入背包物品的总质量不大于背包的最大载重量。  