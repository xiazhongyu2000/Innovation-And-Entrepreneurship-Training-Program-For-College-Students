# Innovation-and-Entrepreneurship-Training-Program-for-College-Students
大学生创新创业训练计划
## 项目名称：[基于VRP模型的O2O外卖配送路径优化研究](Research_On_O2O_Takeaway_Delivery_Route_Optimization_Based_On_VRP_Model.md)
#### 1.[遗传算法求解0-1背包问题](Genetic_Algorithm_to_Solve_0-1_Knapsack_Problem.md)  
#### 2.[遗传算法求解带时间窗的车辆路径问题](Genetic_Algorithm_for_Solving_Vehicle_Routing_Problem_with_Time_Window.md)  
#### 3.[进化系列算法](Evolutionary_Series_Algorithms.md)  
##### 3.1 [进化系列算法概述](Overview_Of_Evolutionary_Series_Algorithms.md)
##### 3.2 [遗传算法]()
##### 3.3 [进化策略算法]()
##### 3.4 [进化规划算法]()
##### 3.5 [遗传规划算法]()
##### 3.6 [差分进化算法]()
##### 3.7 [分布估计算法]()
#### 4.[多目标优化算法](Multi-objective_Optimization_Algorithm.md)  
##### 4.1 [多目标优化问题]()
##### 4.2 [基于进化策略的多目标优化算法]()
##### 4.3 [基于粒子群算法的多目标优化算法]()
#### [作者：夏忠宇](https://github.com/xiazhongyu2000)  
版权所有，若侵犯了您的权益，请联系我删除相关内容，e-mail:xiazhongyu2000@gmail.com  
#### 技术支持：
1.[LaTeX公式编辑器](https://www.latexlive.com/)  
2.[Typora](https://typoraio.cn/)  
3.[Visual Studio Code](https://code.visualstudio.com/)  
4.[MATLAB](https://www.mathworks.com/)  
5.[Microsoft Windows 11](https://www.microsoft.com/windows/)  
### 参考文献：
 [1]中文版MATLAB 2020从入门到精通：实战案例版/天工在线编著.-北京：中国水利水电出版社，2020.10（2021.11重印） ISBN 978-7-5170-8777-9  
 [2]MATLAB智能优化算法：从写代码到算法思想/曹旺著.-北京：北京大学出版社，2021.8 ISBN 978-7-301-32238-3  
 [3]25个经典的元启发式算法：从设计到MATLAB实现/崔建双主编.-北京：企业管理出版社，2021.3 ISBN 978-7-5164-2297-7  
