# Overview_Of_Evolutionary_Series_Algorithms
### 进化系列算法概述
进化系列算法是由一系列取进化策略而实现的群解元启发式算法，主要包括遗传算法(genetic algorithm,GA)、进化策略算法(evolution strategy,ES)、进化规划算法(evolutionary programming,EP)和遗传规划算法(genetic programming,GP)，以及后来出现的差分进化算法(differential evolution,DE)、分布估计算法(distribution estimation,DE)等。